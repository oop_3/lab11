package com.kaewmanee.week11;

public class Bat extends Animal implements FlyAble{

    public Bat(String name) {
        super(name, 2);
    }

    @Override
    public void takeoff() {
        System.out.println(this+"  takeoff.");
        
    }

    @Override
    public void fly() {
        System.out.println(this+"  fly.");
        
    }

    @Override
    public void landing() {
        System.out.println(this+"  landing.");
        
    }

    @Override
    public void sleep() {
        System.out.println(this+"  sleep.");
        
    }

    @Override
    public void eat() {
        System.out.println(this+"  eat.");
        
    }
    
    @Override
    public String toString() {
        return "Bat ("+this.getName()+")";
    }
    
}
