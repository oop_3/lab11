package com.kaewmanee.week11;

public interface FlyAble {
    public void takeoff();
    public void fly();
    public void landing();
    
}
