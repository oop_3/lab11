package com.kaewmanee.week11;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Bird bird1 = new Bird("Urai");
        // bird1.eat();
        // bird1.sleep();
        // bird1.takeoff();
        // bird1.fly();
        // bird1.landing();
        // bird1.walk();
        // bird1.run();

        Plane plane1 = new Plane("Boeing777","777");
        // plane1.takeoff();
        // plane1.fly();
        // plane1.landing();

        Superman man1 = new Superman("Clark");
        // man1.sleep();
        // man1.eat();
        // man1.takeoff();
        // man1.fly();
        // man1.landing();
        // man1.run();
        // man1.walk();
        // man1.swim();

        Human realMan = new Human("Pim");
        // realMan.sleep();
        // realMan.eat();
        // realMan.run();
        // realMan.walk();
        // realMan.swim();

        Bat bat1 = new Bat("Batboy");
        // bat1.sleep();
        // bat1.eat();
        // bat1.takeoff();
        // bat1.fly();
        // bat1.takeoff();

        Snake snake = new Snake("NongNgoo");
        // snake.eat();
        // snake.sleep();
        // snake.crawl();
        // snake.swim();

        Rat rat = new Rat("Remy");
        // rat.run();
        // rat.sleep();
        // rat.walk();
        // rat.eat();
        // rat.swim();

        Dog dog = new Dog("Dibby");
        // dog.run();
        // dog.walk();
        // dog.eat();
        // dog.sleep();
        // dog.swim();

        Cat cat = new Cat("Kitty");
        // cat.run();
        // cat.walk();
        // cat.eat();
        // cat.sleep();
        // cat.swim();

        Fish fish = new Fish("Nemo");
        // fish.eat();
        // fish.sleep();
        // fish.swim();

        Crocodile cropped = new Crocodile("Baby");
        // cropped.eat();
        // cropped.sleep();
        // cropped.crawl();
        // cropped.swim();

        Submarine submarine = new Submarine("Dumnarm","Tuu");
        // submarine.swim();

        Bus bus = new Bus("Buddy","lambo");
        // System.out.println(bus);

        FlyAble[] flyable = {plane1,bat1,bird1,man1};
        for(int i=0;i<flyable.length;i++) {
            flyable[i].takeoff();
            flyable[i].fly();
            flyable[i].landing();
        }
        WalkAble[] walkable = {man1,bird1,cat,dog,rat,realMan};
        for(int i=0;i<walkable.length;i++) {
            walkable[i].run();
            walkable[i].walk();
        }
        Crawlable[] crawlable = {snake,cropped};
        for(int i=0;i<crawlable.length;i++){
            crawlable[i].crawl();
        }
        SwimAble[] swimable = {man1,realMan,rat,cat,dog,fish,submarine};
        for(int i=0;i<swimable.length;i++){
            swimable[i].swim();
        }



    }
}
