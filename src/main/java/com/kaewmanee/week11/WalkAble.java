package com.kaewmanee.week11;

public interface WalkAble {
    public void walk();
    public void run();
    
}
