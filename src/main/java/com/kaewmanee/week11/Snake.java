package com.kaewmanee.week11;

public class Snake extends Animal implements Crawlable,SwimAble{

    public Snake(String name) {
        super(name, 0);
    }

    @Override
    public void sleep() {
        System.out.println(this.toString()+"  sleep.");
    }

    @Override
    public void eat() { 
        System.out.println(this.toString()+"  eat.");
    }

    @Override
    public String toString() {
        return "Snak ("+this.getName()+")";
    }

    @Override
    public void crawl() {
        System.out.println(this.toString()+"  crawl.");
        
    }

    @Override
    public void swim() {
        System.out.println(this.toString()+"  swim.");
        
    }
    
}
