package com.kaewmanee.week11;

public abstract class Vehicle {
    private String name;
    private String engine;
    public Vehicle(String name, String engine) {
        this.name = name;
        this.engine = engine;
    }
    public String getName() {
        return name;
    }
    public String getEngine() {
        return engine;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setEngine(String engine) {
        this.engine = engine;
    }
    @Override
    public String toString() {
        return "Vehicle ("+name+")";
    }

    
}
