package com.kaewmanee.week11;

public class Crocodile extends Animal implements Crawlable,SwimAble,WalkAble{

    public Crocodile(String name) {
        super(name, 4);
    }

    @Override
    public void sleep() {
        System.out.println(this+"  sleep.");
        
    }

    @Override
    public void eat() {
        System.out.println(this+"  eat.");
        
    }
    @Override
    public String toString() {
        return "Crocodile ("+this.getName()+")";
    }


    @Override
    public void swim() {
        System.out.println(this+"  swim.");
        
    }
    @Override
    public void crawl() {
        System.out.println(this.toString()+"  crawl.");
        
    }

    @Override
    public void walk() {
        System.out.println(this+"  walk.");
        
    }

    @Override
    public void run() {
        System.out.println(this+"  run.");
        
    }
    
}
